const fs = require('fs')
const path = require('path')




const problem1 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data);
            ids = [2, 13, 23]
            const result = ids.map((eachID) => {
                const resultID = employeeData.employees.filter((each) => {
                    return each.id == eachID
                })
                return resultID

            })
            // console.log(result);
            let datId = JSON.stringify(result)
            fs.appendFile("firstResult.json", datId, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("data written successfully");
                    problem2()
                }
            })
        }
    })
}


const problem2 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data)

            const groupCompany = employeeData.employees.reduce((group, current) => {
                if (group.hasOwnProperty(current.company)) {
                    group[current.company].push(current)
                } else {
                    group[current.company] = [current]
                }
                return group
            }, {})
            let datCompany = JSON.stringify(groupCompany)
            fs.appendFile("secondResult.json", datCompany, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("data for 2nd function written successfully");
                    problem3()
                }
            })
        }
    })
}


const problem3 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data)

            const powerPuffBrigade = employeeData.employees.reduce((group, current) => {
                if (current.company == "Powerpuff Brigade") {
                    group.push(current)
                }
                return group
            }, [])
            // console.log(powerPuffBrigade);
            let dataPowerPuff = JSON.stringify(powerPuffBrigade)
            fs.appendFile("thirdResult.json", dataPowerPuff, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("data for 3rd function written successfully");
                    problem4()
                }
            })
        }
    }
    )
}


const problem4 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data)
            const newData = employeeData.employees.filter((each) => {
                return each.id != 2
            })
            let employeeDataWithout2 = JSON.stringify(newData)
            fs.appendFile("fourthResult.json", employeeDataWithout2, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("data for 4rd function written successfully");
                }
            })
        }
    })

}

const problem5 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data)
            employeeData.employees.sort((firstEmployee, secondEmployee) => {
                if (firstEmployee.company > secondEmployee.company) {
                    return 1
                }
                else if (firstEmployee.company < secondEmployee.company) {
                    return -1
                }
                else {
                    return firstEmployee.id - secondEmployee.id
                }
            })
            let employeeDataSorted = JSON.stringify(employeeData.employees)
            fs.appendFile("fifthResult.json", employeeDataSorted, (err, data) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log("data for 5rd function written successfully");
                }
            })

        }
    })
}

const problem7 = () => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.log(err)
        }
        else {
            let employeeData = JSON.parse(data)
            const newDataEmployee=employeeData.employees.map((eachEmployee)=>{
                if(eachEmployee.id%2==0){
                    let newDate=new Date()

                    eachEmployee.birthdate=newDate.toDateString().slice(4)
                }
            })
            console.log(employeeData);
        }
    })
}
// problem1()
problem7()